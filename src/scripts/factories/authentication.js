var Authentication = function($cookies) {
  var auth = {};

  auth.setAuth = function(newAuth) {
    $cookies.put('accessToken', newAuth["access-token"]);
    $cookies.put('client', auth.client = newAuth["client"]);
    $cookies.put('uid', auth.uid = newAuth["uid"]);
  };

  auth.getAuth = function() {
    auth.accessToken = $cookies.get('accessToken');
    auth.client = $cookies.get('client');
    auth.uid = $cookies.get('uid');

    return auth;
  }

  return auth;
};

export { Authentication };
var Home = function($scope, $http, $httpParamSerializer, authentication) {

  $scope.tipos = [];
  $scope.tiposSelecionados = [];
  $scope.showEmpresa = false;

  $scope.search = {
    nome: '',
    tipo: [],
  };

  var auth = authentication.getAuth();

  var getById = function(idEmpresa) {
    var req = {
      method: 'GET',
      url: `http://54.94.179.135:8090/api/v1/enterprises/${idEmpresa}`,
      headers: {
        "Content-type": "application/json",
        "access-token": auth.accessToken,
        "client": auth.client,
        "uid": auth.uid
      },
    }

    $http(req).then((result) => {
      $scope.empresaSelecionada = result.data;
    });
  }

  var getTypes = function() {
    $scope.empresas.forEach(empresa => {
      if ($scope.tipos.filter(tipo => tipo.id === empresa.enterprise_type.id).length === 0) {
        $scope.tipos.push(empresa.enterprise_type)
      }
    });
  }

  var filterTipos = function(busca) {
    var tiposFiltro = [];
    $scope.tipos.forEach(tipo => {
      if(tipo.enterprise_type_name.toUpperCase().includes(busca.toUpperCase()))
        tiposFiltro.push(tipo.id);
    });
    return tiposFiltro;
  }

  $scope.resetShowEmpresa = function() {
    $scope.showEmpresa = !$scope.showEmpresa;
    $scope.empresaSelecionada = {};
  }

  $scope.clearSearch = function() {
    $scope.search = {
      nome: '',
      tipo: [],
    };
    $scope.buscar();
  }

  $scope.buscar = function() {
    var enterprise_types;
    if ($scope.search.tipo.length > 0) enterprise_types = filterTipos($scope.search.tipo);

    var req = {
      method: 'GET',
      url: `http://54.94.179.135:8090/api/v1/enterprises`,
      headers: {
        "Content-type": "application/json",
        "access-token": auth.accessToken,
        "client": auth.client,
        "uid": auth.uid
      },
      params: {
        name: $scope.search.nome,
        enterprise_types,
      }
    }

    $http(req).then((result) => {
        $scope.empresas = result.data.enterprises;
        if($scope.tipos.length === 0) getTypes();
      }
    );
  }


  $scope.showDetails = function(idEmpresa) {
    getById(idEmpresa);
    $scope.showEmpresa = true;
  }

  $scope.buscar();
}

export { Home };
var Login = function($scope, $http, toastr, $location, authentication) {

  $scope.data = {
    email: "",
    password: "",
  };

  $scope.entrar = function() {
    var req = {
      method: 'POST',
      url: "http://54.94.179.135:8090/api/v1/users/auth/sign_in",
      headers: {
        "Content-type": "application/json",
      },
      data: $scope.data
    }

    $http(req)
      .then((result) => {
        authentication.setAuth(result.headers());
        $location.path('/home');
      })
      .catch((error) => {
        if (error.status === 401) {
          toastr.warning('E-mail ou senha inválidos. Por favor tente novamente.');
        } else {
          toastr.error('Ocorreu um erro inesperado. Por favor tente novamente.');
        }
      });
  }
}

export { Login };
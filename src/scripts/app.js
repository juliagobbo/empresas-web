import angular from 'angular';
import ngResource from 'angular-resource';
import ngRoute from 'angular-route';
import ngCookies from 'angular-cookies';
import ngAnimate from 'angular-animate';
import toastr from 'angular-toastr';

import '../styles/app.scss';

// controllers
import { Login } from './controllers/login';
import { Home } from './controllers/home';

import { Authentication } from './factories/authentication';

var empresasWeb = angular.module('empresasWeb', [
  ngResource,
  ngRoute,
  ngCookies,
  toastr,
  ngAnimate
]);

empresasWeb.config(function ($routeProvider, $locationProvider, $qProvider) {

  $routeProvider
    .when("/login", {
      template: require('../layouts/login.pug'),
      controller: "login",
    })
    .when("/home", {
      template: require('../layouts/home.pug'),
      controller: "home",
      authenticated: true
    })
    .otherwise({
      redirectTo : '/login'
    });

	$locationProvider.html5Mode(false).hashPrefix('');

  $qProvider.errorOnUnhandledRejections(false);

});

empresasWeb.run(function($rootScope, $location, authentication) {
  $rootScope.$on('$routeChangeStart', function(event, next, current) {
    if (next.$$route.authenticated) {
      var user = authentication.getAuth();
      if(!user.accessToken) $location.path('/login');
    }
  });
});


empresasWeb.controller('login', Login);
empresasWeb.controller('home', Home);

empresasWeb.factory('authentication', Authentication);